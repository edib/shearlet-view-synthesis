-------------------------------------------------------------------

 Demo software for Epipolar plane image reconstruction using Shearlet transform
	Public release (6 Sept. 2017)

-------------------------------------------------------------------

Copyright (c) 2016-2017 Tampere University of Technology. 
All rights reserved.
This work should be used for nonprofit purposes only.

Authors:
Suren Vagharshakyan
Robert Bregovic
Gotchev Atanas

Web page:
http://www.cs.tut.fi/~vagharsh/EPISparseRec.html

-------------------------------------------------------------------
 Contents
-------------------------------------------------------------------

The package comprises these files

*) Demo.m			: Reconstruction demo [1,2]
*) epi.mat			: Example of RGB epipolar plane image
*) shearlets\			: Shearlet transform construction using method presented in Shearlab3D [3]
*) EpiSparseRec75.mexw64	: Reconstruction algorithm implementation compiled with CUDA 7.5
*) EpiSparseRec80.mexw64	: Reconstruction algorithm implementation compiled with CUDA 8.0


-------------------------------------------------------------------
 Requirements
-------------------------------------------------------------------

*) MS Windows (64 bit)
*) Matlab (test on R2016a)
*) Microsoft Visual C++ 2013 Redistributable Package
	It can be downloaded from: https://www.microsoft.com/en-us/download/details.aspx?id=40784
*) Multithreaded CPU implementation is based on OPENMP and FFTW libraries (www.fftw.org)
*) Cuda 7.5/8.0 compatible GPU. (tested on Geforce Titan X)
	It can be downloaded from: https://developer.nvidia.com/cuda-zone


-------------------------------------------------------------------
 References
-------------------------------------------------------------------

[1] S. Vagharshakyan, R. Bregovic and A. Gotchev, "Light Field Reconstruction Using Shearlet Transform," in IEEE Trans. on PAMI, 2017. doi: 10.1109/TPAMI.2017.2653101

[2] S. Vagharshakyan, R. Bregovic and A. Gotchev, "Accelerated Shearlet-Domain Light Field Reconstruction," in IEEE J. of STSP, 2017. doi: 10.1109/JSTSP.2017.2738617

[3] G. Kutyniok, W.-Q. Lim, R. Reisenhofer, "ShearLab 3D: Faithful Digital Shearlet Transforms Based on Compactly Supported Shearlets", ACM Trans. Math. Software 42 (2016), Article No.: 5.

-------------------------------------------------------------------
 Disclaimer
-------------------------------------------------------------------

Any unauthorized use of these routines for industrial or profit-oriented activities is expressively prohibited. By downloading and/or using any of these files, you implicitly agree to all the terms of the TUT limited license:
http://www.cs.tut.fi/~vagharsh/Legal_Notice.pdf
