% load EPI
load('epi.mat', 'epi');

% input EPIs
inputEPI = single(epi);

% mask of input values
maskEPI = single(ones(size(inputEPI)));

% parameters
drange = 12;    % disparity range (maximum disparity after shearing by disparity minimum) - integer
dmin = -3;      % -1 * (disparity minimum value) applied in advance, 
                % dmin/drange applied on reconstructed EPI before output,
                % shearing operator is implemented using row-wise cubic interpolation - float
thmax = 2;      % threshold maximum value - float
thmin = 0.02;   % threshold minimum value - float
numit = 20;     % number of iterations - integer

alpha = 10;         % acceleration parameter, typical less than drange - float

% output decimation factor - integer
% 0 - padded
% 1 - crop only padding
% 2 - return every 2-nd reconstructed row, ....
outdec = 1;

% lowpass initial estimation
% 0 - initial estimation is not changed
% 1 - initial estimation is lowpass filtered
lowpass = 1;

% normalize values of input data (should be associated with threshold maximum and minimum values)
% (EPI - min(EPI(:)))/(max(EPI(:)) - min(EPI(:)))
% normalization is used only during reconstruction
% output EPI is rescaled back to original range of values. 
normalize = 1;

% typical processing of multiple EPIs is distributed between GPUs using parfor.
% computeId - scalar defining processing unit
% for GPU
% computeId = 1 or 2 ... gpuDeviceCount(). index of the GPU
% for CPU (using fftw3, much slower than GPU)
% computeId = 0 corresponds to run on CPU with omp_get_num_procs() threads. 
% computeId = -N corresponds to run on CPU with N threads.
computeId = 0; 

% construct shearlet transform [3]
addpath('./shearlets');

% scale level 4 corresponding to maximum disparity range 2^4. 
% large disparity range requires more scale levels which increases working area and processing time.
scalelvl = 1:4;
sys = constructShearlet([123, 123], scalelvl, [128 512]); 

% scalelvl = 1:2;
% sys = constructShearlet([25, 25], scalelvl, [32 512]); 

% scalelvl = 1:5;
% sys = constructShearlet([255, 255], scalelvl, [256 512]); 

% initial estimation for reconstructed epi
initEst = single([]);

% fixed alpha parameter with double overrelaxation presented in STPT [2].
mode = 'F'; 
% original accelerated method presented in PAMI [1], slow but doesn't depend on alpha - parameter
%mode = 'A';

H = 'H'; % horizontal EPI
% H = 'V'; % vertical EPI 
% % transform structure - sys should be transposed manually, i.e.
% sys.dec = permute(sys.dec, [2 1 3]); 
% sys.rec = permute(sys.rec, [2 1 3]); 
% sys.w = permute(sys.w, [2 1 3]); 

params = [ drange thmax thmin numit alpha dmin outdec lowpass normalize ];
%{
if (computeId <= 0) % cpu
    recEpi = EpiSparseRec75(sys, inputEPI, maskEPI, initEst, params, mode, H, computeId);
else                % gpu
    g = gpuDevice();
    dv = g.DriverVersion;
    if (dv == 7.5)
        recEpi = EpiSparseRec75(sys, inputEPI, maskEPI, initEst, params, mode, H, computeId);
    end
    if (dv == 8.0)
        recEpi = EpiSparseRec80(sys, inputEPI, maskEPI, initEst, params, mode, H, computeId);
    end
end
%}
recEpi = EpiSparseRec80(sys, inputEPI, maskEPI, initEst, params, mode, H, computeId);
recEpi = uint8(recEpi);

%%
figure,
subplot(2, 1, 1)
imagesc(1:size(inputEPI, 2), 1:drange:(size(inputEPI, 1)-1)*drange+1, uint8(inputEPI)); axis image; title('Input EPI');
set(gca, 'YTick', 1:drange:(size(inputEPI, 1)-1)*drange+1);
subplot(2, 1, 2)
imagesc(1:size(recEpi, 2), 1:size(recEpi, 1), uint8(recEpi)), axis image; title('Reconstructed EPI');

%%
% [1] S. Vagharshakyan, R. Bregovic and A. Gotchev, "Light Field Reconstruction Using Shearlet Transform," in IEEE Trans. on PAMI, 2017. doi: 10.1109/TPAMI.2017.2653101
% [2] S. Vagharshakyan, R. Bregovic and A. Gotchev, "Accelerated Shearlet-Domain Light Field Reconstruction," in IEEE J. of STSP, 2017. doi: 10.1109/JSTSP.2017.2738617
% [3] G. Kutyniok, W.-Q. Lim, R. Reisenhofer, "ShearLab 3D: Faithful Digital Shearlet Transforms Based on Compactly Supported Shearlets", ACM Trans. Math. Software 42 (2016), Article No.: 5.
