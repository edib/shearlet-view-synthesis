LFDir = 'D:\MMSP_light_field_dataset/';


LFNamePrefix='';

InterpPositionMode = 1; % 0 : round calibrated positions to interpolate positions from a regular grid (checks the rounded calibrated positions corresponds to the expected grid).
                        % 1 : keep calibrated positions and interpolate positions between them.
PreEstMethod='LFEPICNN';%shearlet LFCNN LFEPICNN

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Default parameter values      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
flipU=false;flipV=false; crop = [];rescale = 1; sizeDivisor = 1;
downwardConvention = true;
LFEstimIdOffset = 0;

switch PreEstMethod
    case 'shearlet'
        LFDirEstim = './InterpShearlets';
        LFEstimSuffix = '_FullSize100iter';
        LFEstimFmt = '_';
    case 'LFCNN'
        LFDirEstim = 'C:\Users\Mikael Le Pendu\Desktop\SIGGRAPHAsia16_ViewSynthesis_Code_v2.0\Results\';
        LFEstimSuffix = '-8x8';
        LFEstimFmt='/';
    case 'LFEPICNN'
        LFDirEstim = 'C:\Users\Mikael Le Pendu\Desktop\lfepicnn-master\Results\';
        LFEstimSuffix = '-7x7';
        LFEstimFmt='/';
    otherwise
        error('unkown pre-estimation method');
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%{
LFName = 'butterfly';
inputConfig = '2x2';%'2x2' '3x3' '5x5' '9x9' 'border'
estimConfig = 'none'; %'none' 'border' 'full'
rescale = 1;
crop = [0 0 0 0];%[160 51 160 51];% %[CropLeft,CropRight,CropTop,CropBottom].
uRange = [0:8];%[6:10];
vRange = [0:8];%[6:10];
downwardConvention = false;
%}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%{
LFName = 'Illum_Fruits';% Illum_Fruits , Illum_Figurines EPFL_Friends_1 EPFL_Vespa
inputConfig = '3x3';%'2x2' '3x3' '5x5' '9x9' 'border' 'full'
estimConfig = 'none'; %'none' 'border' 'full'
rescale = 1;
crop = [2 2 2 2];%[12 12 12 12]; %[CropLeft,CropRight,CropTop,CropBottom].
uRange = [4:10];%
vRange = [4:10];%
LFEstimIdOffset = 3;
%}

%{
LFDirIn = 'D:\LFData\';
LFName = 'neuronsx20';%
inputConfig = '2x2';%'2x2' '3x3' '5x5' '9x9' 'border'
estimConfig = 'none'; %'none' 'border' 'full'
rescale = 1;
crop = [12 12 12 12]; %[CropLeft,CropRight,CropTop,CropBottom].
uRange = [4:11];%
vRange = [4:11];%
LFEstimIdOffset = 3;
%}

%{
LFDirIn = 'D:\LFData\LytroStanford/';
LFName = 'Vespa_gamma'; %Fruits_gamma Rock_gamma Cars Cars_gamma Friends_1_gamma Figurines_gamma Vespa_gamma Books_gamma Bikes_gamma
inputConfig = '3x3';%'2x2' '3x3' '5x5' '9x9' 'border'
estimConfig = 'none'; %'none' 'border' 'full'
rescale = 1;
%crop = [1 1 1 1];
crop = [0 0 0 0];%[12 12 12 12]; %[CropLeft,CropRight,CropTop,CropBottom].
uRange = [1:7];%[1:8];%
vRange = [1:7];%[1:8];%
%}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Parameters for FDL calibration + construction
padType = 'symmetric';	%'replicate' or 'symmetric'
padSizeX=15;			%Number of padded pixels on left and right borders.
padSizeY=15;			%Number of padded pixels on top and bottom borders.
lambdaCalib = 1;
lambdaConstruct = .1;%1;%
numLayers = 30;%20;%

if(strcmp(inputConfig,'2x2') || (strcmp(inputConfig,'3x3') && ~strcmp(LFName,'butterfly')) && strcmp(estimConfig,'none'))
    lambdaConstruct=1;
    numLayers=20;
end
%if(strcmp(estimConfig,'border'))
%    lambdaCalib=.5;
%    lambdaConstruct=.01;
%end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Input Configuration
LFNameEstim = [LFName LFEstimFmt inputConfig LFEstimSuffix];
nU=length(uRange);
nV=length(vRange);

uvListOrg = createUVList(inputConfig, uRange, vRange);
uvListEstim = createUVList(estimConfig, uRange, vRange);
uvListEstim = setdiff(uvListEstim, uvListOrg, 'rows');
uvList = [uvListOrg;uvListEstim];

numEstimViews = size(uvListEstim,1);
numInitViews = size(uvList,1);

%% Load Light Field data
LF = loadLFuvList([LFDirIn LFName],LFNamePrefix,'png',uvListOrg, crop, rescale);
LF(:,:,:,end+1:end+numEstimViews) = loadLFuvList([LFDirEstim LFNameEstim],LFNamePrefix,'png',uvListEstim-LFEstimIdOffset, [], rescale);
LF = double(LF)/255;
imgSize = [size(LF,1),size(LF,2)];
nChan = size(LF,3);
LF_rec = zeros(imgSize(1),imgSize(2),nChan,nU*nV,'uint8');



start=tic;
%% Prepare variables
fullResX = padSizeX*2+imgSize(2);
fullResY = padSizeY*2+imgSize(1);
%frequency grid:
[wx,wy,xC,yC] = GenerateFrequencyGrid(fullResX,fullResY);
%window profile:
Window = gpuArray(single(GenerateWindowIntensity(fullResX,fullResY,padSizeX,padSizeY, 'hann')));

%% Pre-process input data (Padding/Windowing + Fourier Tramsform )
ViewsFFT = zeros([numInitViews,nChan,fullResY,fullResX],'single');
for idView=1:numInitViews
    orgPad = gpuArray(padarray(single(LF(:,:,:,idView)),[padSizeY padSizeX],padType));
    orgPad = bsxfun(@times, orgPad, Window);
    %orgPad(:,:,1) = Window.*wiener2(gather(orgPad(:,:,1)),[10 10],(1-Window)*.1);
    %orgPad(:,:,2) = Window.*wiener2(gather(orgPad(:,:,2)),[10 10],(1-Window)*.1);
    %orgPad(:,:,3) = Window.*wiener2(gather(orgPad(:,:,3)),[10 10],(1-Window)*.1);
    %orgPad(isnan(orgPad))=0;
    ViewsFFT(idView,:,:,:) = gather(permute(fftshift(fftshift(fft2(orgPad),1),2),[3 1 2]));
end
clear Window orgPad




%% FDL Calibration
%[U,V] = meshgrid(uRange(1:3:end)-mean(uRange),mean(vRange)-vRange(1:3:end));U=U(:);V=V(:);
%[Px,Py,U,V,D,~,~,~,~,~,residN2,gradN2]=CalibrateFDL_UVD_gpu(ViewsFFT, wx, wy, numLayers, lambdaCalib,U,V);

[Px,Py,U,V,D,~,~,~,~,~,residN2,gradN2]=CalibrateFDL_UVD_gpu(ViewsFFT, wx, wy, numLayers, lambdaCalib);
[U,V,D]=ScaleParams(U,V,D,nU,nV); %Scaling and centering of the parameters to match approximately an integer grid of views (this step has no effect on the final images).

if(InterpPositionMode==0)
    U(:) = round(U);
    V(:) = round(V);
    uvTh = uvList-mean(uvList,1);
    if(downwardConvention), uvTh(:,2)=-uvTh(:,2);end
    if(isequal([U,V], -uvTh) )
        U=-U;V=-V;D=-D;
    elseif(~isequal([U,V], uvTh))
        warning('Calibrated view positions does not match integer grid.');
    end
    %U=uvTh(:,1);V=uvTh(:,2);D=-D;

    [UFullGrid, VFullGrid] = meshgrid(uRange,vRange);
    UVFullGridIds = [UFullGrid(:) VFullGrid(:)];
    UFullGrid = UFullGrid(:)-mean(uvList(:,1));
    VFullGrid = VFullGrid(:)-mean(uvList(:,2));
end


%D = interp1(D,linspace(5,26,30));
%D=linspace(-1.3,.9,30);

%{
lambdaConstructRange=[.5:.1:2];%140;%270;
avgPSNR = zeros(1,length(lambdaConstructRange));
avgPSNR_c = zeros(1,length(lambdaConstructRange));
for lambdaConstructId=1:numel(lambdaConstructRange)
    lambdaConstruct = lambdaConstructRange(lambdaConstructId);
%}
    
%% FDL Construction
FDL = ComputeFDL_gpu(ViewsFFT, wx, wy, U, V, D, lambdaConstruct);

%% Render + PSNR
if(InterpPositionMode==1)
    [UFullGridIds, VFullGridTIds] = meshgrid(uRange,vRange);
    UFullGrid = griddata(uvList(:,1),uvList(:,2), double(U), UFullGridIds(:),VFullGridTIds(:));
    VFullGrid = griddata(uvList(:,1),uvList(:,2), double(V), UFullGridIds(:),VFullGridTIds(:));
    UVFullGridIds = [UFullGridIds(:) VFullGridTIds(:)];
    clear UFullGridIds VFullGridTIds
end

RMod = RenderModel(FDL, [fullResY,fullResX], [padSizeX, padSizeX, padSizeY, padSizeY], D,0); 
RMod.setRadius(0);
idView=0;
for uId=1:nU
    for vId=1:nV
        idView = idView+1;
        u = UFullGrid(idView);
        v = VFullGrid(idView);
        if(InterpPositionMode==0 && downwardConvention), v=-v;end
        RMod.setPosition(u,v);
        RMod.renderImage();
        LF_rec(:,:,:,idView) = RMod.Image;
    end
end
time=toc(start)


%% Evaluate PSNR (only for intermediate viewpoints)
LF = double(loadLFuvList([LFDirIn LFName],LFNamePrefix,'png',UVFullGridIds, crop, rescale));
LF_rec = double(LF_rec);
psnr = zeros(nU,nV);
psnr_c = zeros(nU,nV);
cropEval = [12 12 12 12] - crop;
idView=0;
for uId=1:nU
    for vId=1:nV
        idView = idView+1;
        u = UVFullGridIds(idView,1);
        v = UVFullGridIds(idView,2);
        if(ismember([u,v],uvListOrg,'rows'))
            psnr(uId,vId)=inf;
            psnr_c(uId,vId)=inf;
            continue;
        end
        psnr(uId,vId) = 10*log10(255^2/mean(reshape(LF(:,:,:,idView)-LF_rec(:,:,:,idView),[],1).^2));
        psnr_c(uId,vId) = 10*log10(255^2/mean(reshape(LF(1+cropEval(3):end-cropEval(4),1+cropEval(1):end-cropEval(2),:,idView)-LF_rec(1+cropEval(3):end-cropEval(4),1+cropEval(1):end-cropEval(2),:,idView),[],1).^2));
    end
end
sprintf('psnr: %0.2f dB / psnr_c: %0.2f dB',mean(psnr(~isinf(psnr))), mean(psnr_c(~isinf(psnr_c))) )


%avgPSNR(lambdaConstructId) = mean(psnr(~isinf(psnr)));
%avgPSNR_c(lambdaConstructId) = mean(psnr_c(~isinf(psnr_c)));
%end

%Video=displayLFMatrix(permute(reshape(LF_rec,prod(imgSize),nChan,[]),[1 3 2]),imgSize,nU,nV,1);



%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function LF = loadLFuvList(path, LFnamePrefix, fileExt, uvList, crop, rescale)

if(~exist('crop','var')),crop=[];end
if(~exist('rescale','var')||isempty(rescale)),rescale=1;end

fDir = dir([path '/']);

if(isempty(uvList))
    LF=[];
    return;
end

IntitData=true;
for idView=1:size(uvList,1)
    str_u = num2str(uvList(idView,1));
    str_v = num2str(uvList(idView,2));
    fName = regexp({fDir.name},['^' LFnamePrefix '0*' str_v '_0*' str_u '[.]' fileExt '$'],'match');
    fName=[fName{:}];
    if(isempty(fName)), error(['Can''t find image file of light field view (u=' str_u ',v=' str_v ') in folder ' path '/']);end
    fName=fName{1};

    %Load view
    TmpImg = imread([path '/' fName]);

    %Size reduction / cropping
    if(~isempty(crop))
        TmpImg = TmpImg(1+crop(3):end-crop(4),1+crop(1):end-crop(2),:);
    end
    if(rescale~=1)
        TmpImg=imresize(TmpImg, rescale);
    end

    if(IntitData)
        LF = zeros([size(TmpImg),size(uvList,1)],class(TmpImg));
    end
    LF(:,:,:,idView) = TmpImg;

    IntitData=false;
end
end


function uvList = createUVList(inputConfig, uRange, vRange)
    nU=length(uRange);
    nV=length(vRange);
    switch(inputConfig)
        case '2x2'
            stepU=nU-1; stepV=nV-1;
        case '3x3'
            stepU=(nU-1)/2; stepV=(nV-1)/2;
        case '5x5'
            stepU=(nU-1)/4; stepV=(nV-1)/4;
        case '9x9'
            stepU=(nU-1)/8; stepV=(nV-1)/8;
        case 'border'
            uvList = [[uRange, uRange(end)*ones(1,nV-1), uRange(end-1:-1:1), uRange(1)*ones(1,nV-2)]; [vRange(1)*ones(1,nU), vRange(2:end), vRange(end)*ones(1,nU-1), vRange(end-1:-1:2)]]';
        case 'full'
            stepU=1; stepV=1;
        case 'none'
            stepU=0; stepV=0;
        otherwise
            warning('Unknown configuration -> using full.');
            stepU=1; stepV=1;
    end
    if(~strcmp(inputConfig,'border'))
        [uids,vids]=meshgrid(uRange(1:stepU:end),vRange(1:stepV:end));uvList = [uids(:), vids(:)];%Nx angular super-res
    end
end


