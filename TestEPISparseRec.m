addpath(genpath('EPISparseRec'))
LFDir = 'D:\MMSP_light_field_dataset/';
LFNamePrefix='';
RootDir = './InterpShearlets';
RefDir = './InterpShearletsReference';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


Configs = {'2x2','full';
           '3x3','full';
           '5x5','full';
           %'border','full';
           %'2x2','border';
           %'3x3','border';
           %'5x5','border'
           };

LFNames = {'Greek','Sideboard','Buddha','StillLife','Butterfly'};

for LFName = LFNames
LFName = LFName{:}

for configId = 1:size(Configs,1)
    inputConfig = Configs{configId,1};
    outputConfig = Configs{configId,2};

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Default parameter values      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
flipU=false;flipV=false; crop = [];rescale = 1; sizeDivisor = 1;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%            LF parameters            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%{
LFName = 'truck'; flipV=true;
rescale = 1/2.5;
uRange = [0:16];%[6:10];
vRange = [0:16];%[6:10];
drange = 12;    % disparity range (maximum disparity after shearing by disparity minimum) - integer
dmin = -3;      % -1 * (disparity minimum value) applied in advance, 
                % dmin/drange applied on reconstructed EPI before output,
                % shearing operator is implemented using row-wise cubic interpolation - float
alpha = 10;         % acceleration parameter, typical less than drange - float
%}
%{
LFName = 'Illum_Fruits';
rescale = 1;
crop = [1 1 1 1];
uRange = [3:11];%[6:10];
vRange = [3:11];%[6:10];
knownUStep=4; knownVStep=4;
drange = 4;    % disparity range (maximum disparity after shearing by disparity minimum) - integer
dmin = -.3;      % -1 * (disparity minimum value) applied in advance, 
                % dmin/drange applied on reconstructed EPI before output,
                % shearing operator is implemented using row-wise cubic interpolation - float
alpha = 2;     % acceleration parameter, typical less than drange - float
%}
%%{
%LFName = 'Butterfly';
rescale = 1;
crop = [];%[160 160 51 51];
warning('CHANGING URANGE AND VRANGE')
% uRange = [0:8];%[6:10];
% vRange = [0:8];%[6:10];
%inputConfig = '2x2'; %'2x2' '3x3' '5x5' 'border'
%outputConfig = 'full'; %'full' 'border'
warning('CHANGING DISPMIN AND DISPMAX')
% dispMin=-1;dispMax=1;
switch LFName
    case {'Butterfly','Buddha','StillLife','Greek','Sideboard'}
        uRange = 0:8;
        vRange = 0:8;
    case {'lego_knights','tarot'}
        uRange = 4:12;
        vRange = 4:12;
end

switch LFName
    case 'Butterfly'
        dispMin = -2;%-1.17;
        dispMax =  1;%0.89;
    case 'Buddha'
        dispMin = -2;%-0.85;
        dispMax =  2;%1.62;
    case 'StillLife'
        dispMin = -3;%-2.71;
        dispMax =  4;%4.03;
    case 'Greek'
        dispMin = -4;%-3.33;
        dispMax =  3;%2.91;
    case 'Sideboard'
        dispMin = -2;%-1.85;
        dispMax =  2;%1.57;
    case 'lego_knights'
        dispMin = -10; %?
        dispMax =  10; %?
    case 'tarot'
        dispMin = -5; %?
        dispMax =  5; %?
end

%}
nU=length(uRange);
nV=length(vRange);
switch(inputConfig)
    case '2x2'
        knownUStep=nU-1; knownVStep=nV-1;           loopV=1:knownVStep:nV; loopU=1:nU;
    case '3x3'
        knownUStep=(nU-1)/2; knownVStep=(nV-1)/2;	loopV=1:knownVStep:nV; loopU=1:nU;
    case '5x5'
        knownUStep=(nU-1)/4; knownVStep=(nV-1)/4;   loopV=1:knownVStep:nV; loopU=1:nU;
    case 'border'
        knownUStep=nU-1; knownVStep=1;              loopV=2:nV-1; loopU=2:nU-1;
        uvIdList = [[1:nU, nU*ones(1,nV-1), nU-1:-1:1, 1*ones(1,nV-2)]; [1*ones(1,nU), 2:nV, nV*ones(1,nU-1), nU-1:-1:2]]';
    otherwise
        warning('unknown configuration => skip reconstruction');
        continue
end
if(~strcmp(inputConfig,'border'))
    [uids,vids]=meshgrid(1:knownUStep:nU,1:knownVStep:nV);uvIdList = [uids(:), vids(:)];%Nx angular super-res
end

drange = ceil(dispMax-dispMin)*knownUStep;    % disparity range (maximum disparity after shearing by disparity minimum) - integer
dmin = floor(dispMin*knownUStep);      % -1 * (disparity minimum value) applied in advance, 
                                % dmin/drange applied on reconstructed EPI before output,
                                % shearing operator is implemented using row-wise cubic interpolation - float
alpha = drange*5/6;% acceleration parameter, typical less than drange - float



M_org = loadLF([LFDir LFName],LFName,'png', uRange+1, vRange+1, crop);
M_org = double(M_org);
sz = size(M_org);
imgSize = sz(1:2);
nChan = sz(3);
nV = sz(4);
nU = sz(5);
M_org = permute(M_org,[1,2,4,5,3]);
M_org = reshape(M_org,[imgSize, nV, nU, nChan]);
if(flipU), M_org = M_org(:,:,:,end:-1:1,:);end
if(flipV), M_org = M_org(:,:,end:-1:1,:,:);end

RefDir_ = [RefDir '/' LFName '/'];
mkdir(RefDir_)
saveRecImages(RefDir_, uRange, vRange, M_org);

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%          methods parameters         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
thmax = 2;      % threshold maximum value - float
thmin = 0.02;   % threshold minimum value - float
numit = 100;     % number of iterations - integer


% output decimation factor - integer
% 0 - padded
% 1 - crop only padding
% 2 - return every 2-nd reconstructed row, ....
outdec = 1; 

% lowpass initial estimation
% 0 - initial estimation is not changed
% 1 - initial estimation is lowpass filtered
lowpass = 1;

% normalize values of input data (should be associated with threshold maximum and minimum values)
% (EPI - min(EPI(:)))/(max(EPI(:)) - min(EPI(:)))
% normalization is used only during reconstruction
% output EPI is rescaled back to original range of values. 
normalize = 1;

% typical processing of multiple EPIs is distributed between GPUs using parfor.
% computeId - scalar defining processing unit
% for GPU
% computeId = 1 or 2 ... gpuDeviceCount(). index of the GPU
% for CPU (using fftw3, much slower than GPU)
% computeId = 0 corresponds to run on CPU with omp_get_num_procs() threads. 
% computeId = -N corresponds to run on CPU with N threads.
computeId = 1;

% scale level 4 corresponding to maximum disparity range 2^4. 
% large disparity range requires more scale levels which increases working area and processing time.
scalelvl = 1:4;
sz = [123, 123];
bigsz = [128 2^(ceil(log(max(imgSize))/log(2)))];%max(imgSize)];
sys = constructShearlet(sz, scalelvl, bigsz);

% warning('CHANGING SCALE LVL')
% scalelvl = 1:5;
% sz = [255, 255];
% bigsz = [512 2^(ceil(log(max(imgSize))/log(2)))];%max(imgSize)];

% sys = constructShearlet(sz, scalelvl, bigsz);
% scalelvl = 1:2;
% sys = constructShearlet([25, 25], scalelvl, [32 512]); 
% scalelvl = 1:5;
% sys = constructShearlet([255, 255], scalelvl, [256 512]); 

% initial estimation for reconstructed epi
initEst = single([]);

% fixed alpha parameter with double overrelaxation presented in STPT [2].
mode = 'F'; 
% original accelerated method presented in PAMI [1], slow but doesn't depend on alpha - parameter
% mode = 'A';

params = [ drange thmax thmin numit alpha dmin outdec lowpass normalize ];
recUSize = floor(drange)*((nU-1)/knownUStep)+1;
recVSize = floor(drange)*((nV-1)/knownVStep)+1;
recUStep = (recUSize-1)/(nU-1); if(recUStep~=round(recUStep)), error('incompatible output size');end
recVStep = (recVSize-1)/(nV-1); if(recVStep~=round(recVStep)), error('incompatible output size');end


%% Horizontal View interpolation with horizontal epipolar images
H = 'H'; % vertical EPI
maskEPI = single(ones(length(1:knownUStep:nU),imgSize(2),nChan));
M_rec = zeros(imgSize(1),imgSize(2),nV,nU,nChan);
time=0; timeBorder=0;


startTime=tic;

for vId = loopV
    if( vId == 1 || vId == nV ), startTimeBorder=tic;end
for yId = 1:imgSize(1)
%for yId = 200
    inputEPI = single(permute(squeeze(M_org(yId,:,vId,1:knownUStep:nU,:)),[2 1 3]));
    recEPI = EpiSparseRec80(sys, inputEPI, maskEPI, initEst, params, mode, H, computeId);
    M_rec(yId,:,vId,:,:) = permute(recEPI(1:recUStep:end,:,:),[2 1 3]);
end
    if( vId == 1 || vId == nV ), timeBorder=timeBorder+toc(startTimeBorder);end
end
time = time+toc(startTime);

%% Reset the original input views before the vertical pass
for viewId=1:size(uvIdList,1)
    M_rec(:,:,uvIdList(viewId,2),uvIdList(viewId,1),:) = M_org(:,:,uvIdList(viewId,2),uvIdList(viewId,1),:);
end
%Save intermediate pass for the border configuration(the full Lf is already reconstructed).
if(strcmp(inputConfig,'border'))
    ResDir = [RootDir '/' LFName '_border(1stPass) (' datestr(now,'dd.mm.yyyy_HH-MM-SS') ')/'];
    mkdir(ResDir);
    [psnr,psnr_uint8,avgpsnr,avgpsnr_uint8] = computePSNR(M_org, M_rec, uvIdList);
    saveRecImages(ResDir, uRange, vRange, M_rec);
    save([ResDir 'params.mat'],'drange', 'dmin', 'alpha', 'thmax', 'thmin', 'numit', 'outdec', 'lowpass', 'normalize', 'computeId', 'mode', 'scalelvl', 'sz', 'bigsz');
    save([ResDir 'res.mat'], 'psnr','psnr_uint8','avgpsnr','avgpsnr_uint8','time','timeBorder');
end


%% Vertical interpolation of remaining views with vertical epipolar images
H = 'V';
sysV=sys;
sysV.dec = permute(sysV.dec, [2 1 3]);
sysV.rec = permute(sysV.rec, [2 1 3]); 
sysV.w = permute(sysV.w, [2 1 3]);
maskEPI = single(ones(imgSize(1),length(1:knownVStep:nV),nChan));

startTime=tic;

for uId = loopU
    if( uId == 1 || uId == nU ), startTimeBorder=tic;end
for xId = 1:imgSize(2)
    inputEPI = single(squeeze(M_rec(:,xId,1:knownVStep:nV,uId,:)));
    recEPI = EpiSparseRec80(sysV, inputEPI, maskEPI, initEst, params, mode, H, computeId);
    M_rec(:,xId,:,uId,:) = recEPI(:,1:recVStep:end,:);
end
    if( uId == 1 || uId == nU ), timeBorder=timeBorder+toc(startTimeBorder);end 
end

time=time+toc(startTime);


%%
%implay(permute(reshape(M_rec,[imgSize,nU*nV,nChan]),[1 2 4 3])/255);
%implay(reshape(permute(M_rec,[1 2 5 4 3]),[imgSize,nChan,nU*nV])/255);

%% Reset the original input views before saving the results
for viewId=1:size(uvIdList,1)
    M_rec(:,:,uvIdList(viewId,2),uvIdList(viewId,1),:) = M_org(:,:,uvIdList(viewId,2),uvIdList(viewId,1),:);
end
%%
ResDir = [RootDir '/' LFName '_' inputConfig ' (' datestr(now,'dd.mm.yyyy_HH-MM-SS') ')/'];
mkdir(ResDir);
[psnr,psnr_uint8,avgpsnr,avgpsnr_uint8] = computePSNR(M_org, M_rec, uvIdList);
saveRecImages(ResDir, uRange, vRange, M_rec);
save([ResDir 'params.mat'],'drange', 'dmin', 'alpha', 'thmax', 'thmin', 'numit', 'outdec', 'lowpass', 'normalize', 'computeId', 'mode', 'scalelvl', 'sz', 'bigsz');
save([ResDir 'res.mat'], 'psnr','psnr_uint8','avgpsnr','avgpsnr_uint8','time','timeBorder');



end
end





%% utility functions
function [psnr,psnr_uint8,avgpsnr,avgpsnr_uint8] = computePSNR(M_org, M_rec, uvIdList)
    nU = size(M_org,4);
    nV = size(M_org,3);
    psnr = zeros(nU,nV);
    psnr_uint8 = zeros(nU,nV);
    for u=1:nU
        for v=1:nV
            psnr(u,v) = 10*log10(255^2/mean(reshape(M_org(:,:,u,v,:)-M_rec(:,:,u,v,:),[],1).^2));
            psnr_uint8(u,v) = 10*log10(255^2/mean(reshape(M_org(:,:,u,v,:)-double(uint8(M_rec(:,:,u,v,:))),[],1).^2));
        end
    end
    
    [uIds,vIds] = meshgrid(1:nU,1:nV);
    uvIdListInterp = setdiff([uIds(:),vIds(:)],uvIdList,'rows');
    avgpsnr = 0;
    avgpsnr_uint8 = 0;
    for idViews = 1:size(uvIdListInterp,1)
        avgpsnr = avgpsnr + psnr(uvIdListInterp(idViews,1), uvIdListInterp(idViews,2));
        avgpsnr_uint8 = avgpsnr_uint8 + psnr_uint8(uvIdListInterp(idViews,1), uvIdListInterp(idViews,2));
    end
    avgpsnr = avgpsnr/size(uvIdListInterp,1);
    avgpsnr_uint8 = avgpsnr_uint8/size(uvIdListInterp,1);
end

function [avgpsnr,avgpsnr_uint8] = computeAvgPSNR(psnr, psnr_uint8, uvIdList)
    nU = size(psnr,1);
    nV = size(psnr,2);
    [uIds,vIds] = meshgrid(1:nU,1:nV);
    uvIdListInterp = setdiff([uIds(:),vIds(:)],uvIdList,'rows');
    avgpsnr = 0;
    avgpsnr_uint8 = 0;
    for idViews = 1:size(uvIdListInterp,1)
        avgpsnr = avgpsnr + psnr(uvIdListInterp(idViews,1), uvIdListInterp(idViews,2));
        avgpsnr_uint8 = avgpsnr_uint8 + psnr_uint8(uvIdListInterp(idViews,1), uvIdListInterp(idViews,2));
    end
    avgpsnr = avgpsnr/size(uvIdListInterp,1);
    avgpsnr_uint8 = avgpsnr_uint8/size(uvIdListInterp,1);
end

function saveRecImages(ResDir, uRange, vRange, M_rec)
    uId=0;    
    for u=uRange
        uId=uId+1;vId=0;
        for v=vRange
            vId=vId+1;
            imwrite(squeeze(M_rec(:,:,vId,uId,:))/255,[ResDir num2str(v) '_' num2str(u) '.png']);
        end
    end
end


